import React, {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import {AnimatePresence, motion} from "framer-motion";

import {styles} from "../styles";
import {logo, menu, close, moon, sun, closeDark, menuDark} from "../assets";
import {GlobalContext, LanguageContext} from '../context'

import {
  navMotion,
  menuCvMotion,
  menuListMotion,
  itemDownMotion,
  itemRightMotion,
  itemMotion,
  listRightMotion,
  listMotion
} from '../utils/motion'

const Navbar = () => {
  const {darkMode, toggleDarkMode, toggleNational, flag, screenSize, resetAnimation, setResetAnimation} = React.useContext(GlobalContext);
  const {navLinks, cvLabel} = React.useContext(LanguageContext);

  const [active, setActive] = useState("");
  const [toggle, setToggle] = useState(false);
  const [scrolled, setScrolled] = useState(false);

  useEffect(() => {
    setResetAnimation(!resetAnimation)
    setToggle(false)
  }, [screenSize, navLinks, cvLabel, flag, darkMode])

  useEffect(() => {
    const handleScroll = () => {
      const scrollTop = window.scrollY;
      if (scrollTop > 100) {
        setScrolled(true);
      } else {
        setScrolled(false);
        setActive("");
      }
    };

    window.addEventListener("scroll", handleScroll);

    const navbarHighlighter = () => {
      const sections = document.querySelectorAll("section[id]");

      sections.forEach((current) => {
        const sectionId = current.getAttribute("id");
        const sectionHeight = current.offsetHeight;
        const sectionTop =
          current.getBoundingClientRect().top - sectionHeight * 0.2;

        if (sectionTop < 0 && sectionTop + sectionHeight > 0) {
          setActive(sectionId);
        }
      });
    };

    window.addEventListener("scroll", navbarHighlighter);

    return () => {
      window.removeEventListener("scroll", handleScroll);
      window.removeEventListener("scroll", navbarHighlighter);
    };
  }, []);

  return (
    <AnimatePresence>
      <motion.nav
        key={'navbar' + darkMode}
        initial={'hidden'}
        animate={'visible'}
        variants={navMotion}
        className={`menu ${
          styles.paddingX
        } w-full flex items-center py-5 absolute top-0 z-20 ${
          scrolled ? "bg-primary" : "bg-transparent"
        } ${darkMode ? styles.dark : styles.light}`}
      >
        <div
          className="min-w-[14rem] w-full flex justify-between items-center max-w-7xl mx-auto"
        >

          <div className="min-w-fit flex justify-between items-center gap-5 md-flex-1">
            <Link
              to="/"
              className="flex items-center gap-2"
              onClick={() => {
                window.scrollTo(0, 0);
              }}
            >
              <img src={logo} alt="logo" className="w-10 h-10 object-contain"/>
            </Link>
            <motion.ul
              initial="hidden"
              animate="visible"
              variants={listMotion}
              className="list-none hidden md:flex flex-row md:gap-5 lg:gap-10 flex-1"
            >
              {navLinks.map((nav, index) => (
                <motion.button
                  whileHover={{
                    scale: 1.2,
                  }}
                  whileTap={{
                    scale: 0.9
                  }}
                  variants={itemMotion(index)}
                  transition={{type: "spring", stiffness: 400, damping: 15}}
                  key={nav.id + '-button' + resetAnimation}
                >
                  <li
                    key={nav.id}
                    className={`${
                      active === nav.id ? "text-white" : darkMode ? "text-gray-300" : "text-gray-900"
                    } hover:text-white text-[18px] font-medium cursor-pointer`}
                  >
                    <a href={`#${nav.id}`}>{nav.title}</a>
                  </li>
                </motion.button>
              ))}
            </motion.ul>
          </div>

          <motion.div
            initial="hidden"
            animate="visible"
            variants={listRightMotion}
            className="flex justify-between items-center gap-5 mr-1"
          >

            <div>
              <motion.button
                key={'flag' + resetAnimation}
                whileHover={{
                  scale: 1.2,
                }}
                whileTap={{
                  scale: 0.9
                }}
                variants={(screenSize?.xs || screenSize?.sm || screenSize?.md) ? itemDownMotion(1) : itemRightMotion(2)}
                transition={{type: "spring", stiffness: 400, damping: 15}}
              >
                <a href="#" className="flex items-center p-2" onClick={toggleNational}>
                  <img
                    src={flag.svg}
                    alt="flag"
                    className="block h-auto w-7 flex-shrink-0"
                  />
                  <span className={`ml-3 block text-base font-medium ${darkMode ? 'text-gray-300' : 'text-gray-900'}`}>{flag.name}</span>
                </a>
              </motion.button>
            </div>

            <div className="min-w-fit">
              <motion.button
                key={'darkMode' + resetAnimation}
                whileHover={{
                  scale: 1.2,
                }}
                whileTap={{
                  scale: 0.9
                }}
                variants={(screenSize?.xs || screenSize?.sm || screenSize?.md) ? itemDownMotion(2) : itemRightMotion(1)}
                transition={{type: "spring", stiffness: 400, damping: 15}}
                className={`flex mt-auto w-full items-center justify-center rounded-full px-2 py-2 text-base font-medium text-white focus:outline-none ${darkMode ? 'hover:bg-gray-700' : 'hover:bg-white'}`}
              >
                <a href="#" className="-m-2 flex items-center" onClick={toggleDarkMode}>
                  <img
                    src={darkMode ? moon : sun}
                    alt="darkMode"
                    className="block h-auto w-7 flex-shrink-0"
                  />
                </a>
              </motion.button>
            </div>

            <div>
              <motion.button
                key={'cv' + resetAnimation}
                whileHover={{
                  scale: 1.2,
                }}
                whileTap={{
                  scale: 0.9
                }}
                variants={itemRightMotion(0)}
                transition={{type: "spring", stiffness: 400, damping: 15}}
                className={`hidden md:flex mt-auto w-full items-center justify-center rounded-md border border-transparent px-4 py-2 text-base font-medium text-white ${darkMode ? 'bg-gray-500 hover:bg-gray-700' : 'bg-blue-800 hover:bg-blue-600'}`}
              >
                {cvLabel}
              </motion.button>
            </div>

          </motion.div>

          <motion.div
            initial="hidden"
            animate="visible"
            variants={listRightMotion}
            className="min-w-fit md:hidden flex justify-end items-center"
          >
            <motion.button
              key={'menu' + resetAnimation}
              whileHover={{
                scale: 1.2,
              }}
              whileTap={{
                scale: 0.9
              }}
              variants={itemRightMotion(0)}
              transition={{type: "spring", stiffness: 400, damping: 15}}
              className={`flex mt-auto w-full items-center justify-center rounded-full px-1 py-1 text-base font-medium text-white focus:outline-none  ${darkMode ? 'hover:bg-gray-700' : 'hover:bg-gray-100'}`}
            >
              <img
                src={toggle ? (darkMode ? close : closeDark) : (darkMode ? menu : menuDark)}
                alt="menu"
                className="w-[28px] h-[28px] object-contain"
                onClick={() => setToggle(!toggle)}
              />
            </motion.button>
            <div
              className={`${
                !toggle ? "hidden" : "flex"
              } p-6 absolute top-20 right-0 mx-4 my-2 min-w-[12rem] z-10 rounded-xl ${darkMode ? styles.dark : styles.light}`}
            >
              <motion.ul
                initial="hidden"
                animate="visible"
                variants={listMotion}
                className="list-none flex justify-end items-start flex-1 flex-col gap-4"
              >
                <motion.button
                  variants={menuCvMotion(0)}
                  key={'cv-menu' + toggle}
                  whileHover={{
                    scale: 1.0,
                  }}
                  whileTap={{
                    scale: 0.9
                  }}
                  transition={{type: "spring", stiffness: 400, damping: 15}}
                  className={`w-full flex mt-auto items-center justify-center rounded-md border border-transparent px-4 py-2 text-base font-medium text-white focus:outline-none ${darkMode ? 'bg-gray-500 hover:bg-gray-700' : 'bg-blue-800 hover:bg-blue-600'}`}
                >
                  {cvLabel}
                </motion.button>
                {navLinks.map((nav, index) => (
                  <motion.li
                    whileTap={{
                      scale: 0.9
                    }}
                    variants={menuListMotion(index)}
                    transition={{type: "spring", stiffness: 400, damping: 15}}
                    key={nav.id + toggle}
                    className={`w-full font-poppins font-medium cursor-pointer text-[16px] hover:text-white ${
                      active === nav.id ? "text-white" : darkMode ? "text-secondary" : 'text-black'
                    }`}
                    onClick={() => {
                      setToggle(!toggle);
                    }}
                  >
                    <a href={`#${nav.id}`}>{nav.title}</a>
                  </motion.li>
                ))}
              </motion.ul>
            </div>
          </motion.div>
        </div>
      </motion.nav>
    </AnimatePresence>
  );
};

export default Navbar;
