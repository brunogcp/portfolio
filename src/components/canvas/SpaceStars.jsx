import React, {useEffect, useRef, useState} from 'react';
import {extend, Canvas, useFrame, useThree} from '@react-three/fiber';
import {OrbitControls, Stars} from '@react-three/drei';
import * as THREE from 'three';

extend({OrbitControls});

function createStar(xMax, yMax, xMin = 0) {
  const size = Math.floor(Math.random() * 4) + 1;
  const star = {
    x: Math.floor(Math.random() * (xMax - 5 - xMin)) + 5 + xMin,
    y: Math.floor(Math.random() * (yMax - 5)) + 5,
    name: size.toString(),
    size,
  };

  return star;
}

function drawStar(context, star) {
  context.beginPath();
  context.arc(star.x, star.y, star.size, 0, 2 * Math.PI);
  context.shadowColor = '#e6e6e6';
  context.shadowBlur = 4;
  context.fillStyle = '#e6e6e6';
  context.fill();
}

function StarsAnimation({props}) {
  const canvasRef = useRef(null);
  const [visible, setVisible] = useState(true)

  useEffect(() => {
    const canvas = canvasRef.current;

    if (!canvas) return;

    const context = canvas.getContext('2d');
    let start = Date.now();
    let stars = [];

    const handleResize = () => {
      const {innerWidth, innerHeight} = window;
      canvas.width = innerWidth;
      canvas.height = innerHeight;

      stars = [];

      for (let i = 0; i < 100; i++) {
        const star = createStar(innerWidth, innerHeight);
        stars.push(star);
        drawStar(context, star);
      }
    };

    window.addEventListener('resize', handleResize);
    handleResize();

    const animate = () => {
      const {innerWidth, innerHeight} = window;
      requestAnimationFrame(animate);

      const time = Date.now();
      const delta = (start - time) * -0.08;
      start = time;

      context.clearRect(0, 0, innerWidth, innerHeight);

      for (let i = 0; i < 100; i++) {
        stars[i].x -= delta * parseInt(stars[i].name, 10) * 0.2;
        if (stars[i].x < 0) stars[i] = createStar(innerWidth, innerHeight, innerWidth);

        drawStar(context, stars[i]);
      }
    };
    animate();

    function handleVisibilityChange() {
      if (document.hidden) {
        setVisible(false)
      } else {
        setVisible(true)
      }
    }

    document.addEventListener("visibilitychange", handleVisibilityChange);
    return () => {
      window.removeEventListener('resize', handleResize);
      document.removeEventListener("visibilitychange", handleVisibilityChange);
    };
  }, [visible]);

  return (
    <canvas
      ref={canvasRef}
      id="stars"
      aria-hidden
      className={'absolute top-0 left-0 z-[1] w-full'}
    />
  );
}

export default StarsAnimation;
