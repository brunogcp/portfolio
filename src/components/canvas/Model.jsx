import {useAnimations, useGLTF} from "@react-three/drei";
import React, {useEffect} from "react";

const Model = ({
                 path,
                 model = {
                   scale: 0.1,
                   position: [0, 0, 0],
                   rotation: [0, 0, 0],
                   renderOrder: 1
                 },
                 action = {
                   index: 0,
                   play: true
                 },
                 hemisphereLight = {
                   intensity: 1,
                   color: 'black'
                 },
                 spotLight = {
                   position: [-20, 50, 10],
                   angle: 0.12,
                   penumbra: 1,
                   intensity: 1,
                   'shadow-mapSize': 1024,
                   castShadow: true
                 },
                 pointLight = {
                   intensity: 1,
                 }
               }) => {
  const {animations, scene} = useGLTF(path);
  const {names, actions} = useAnimations(animations, scene)

  useEffect(() => {
      if (action.play) {
        actions[names[action.index]]?.play()
      } else {
        actions[names[action.index]]?.stop()
      }
  }, [action, actions, names])

  return (
    <group>
      {hemisphereLight && <hemisphereLight {...hemisphereLight}/>}
      {spotLight && <spotLight {...spotLight}/>}
      {pointLight && <pointLight {...pointLight} />}
      <mesh>
        <primitive
          object={scene}
          {...model}
        />
      </mesh>
    </group>
  );
};

export default Model;
