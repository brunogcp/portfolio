import React, { Suspense } from "react";
import { Canvas } from "@react-three/fiber";
import { OrbitControls, Preload } from "@react-three/drei";

import CanvasLoader from "../Loader";
import Model from "./Model";

const HeroCanvasBottom = () => {
  return (
    <>
      <Canvas
        className={'z-10'}
        frameloop="always"
        shadows
        dpr={[1, 2]}
        camera={{ position: [20, 0, 0], fov: 25 }}
        gl={{ preserveDrawingBuffer: true }}
      >
        <Suspense fallback={<CanvasLoader />}>
          <OrbitControls
            enableZoom={false}
            enablePan={false}
            enableRotate={false}
            maxPolarAngle={Math.PI / 2}
            minPolarAngle={Math.PI / 2}
          />
          <Model
            action={{
              index: 0,
              play: true
            }}
            path={'./holotech_bench/scene.gltf'}
            model={{
              scale: 0.0001,
              position: [-1, -3.75, -1.5],
              rotation: [0, 1, 0],
              renderOrder: 1
            }}
            hemisphereLight={{
              intensity: 0.5,
              color: 'white'
            }}
          />
        </Suspense>
        <Preload all/>
      </Canvas>
    </>
  );
};

export default HeroCanvasBottom;
