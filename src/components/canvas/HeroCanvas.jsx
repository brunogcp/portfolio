import React, {Suspense} from "react";
import {Canvas} from "@react-three/fiber";
import {OrbitControls, Preload} from "@react-three/drei";

import CanvasLoader from "../Loader";
import Model from "./Model";

const HeroCanvas = () => {
  return (
    <Canvas
      className={'z-10 !h-[50%]'}
      frameloop="always"
      shadows
      dpr={[1, 2]}
      camera={{position: [20, 0, 0], fov: 25}}
      gl={{preserveDrawingBuffer: true}}
    >
      <Suspense fallback={<CanvasLoader/>}>
        <OrbitControls
          enableZoom={false}
          enablePan={false}
          maxPolarAngle={Math.PI / 2}
          minPolarAngle={Math.PI / 2}
          minAzimuthAngle={Math.PI / 4} // Define o ângulo mínimo de rotação horizontal
          maxAzimuthAngle={Math.PI / 1.75} // Define o ângulo máximo de rotação horizontal
        />
        <Model
          actionIndex={0}
          path={'./cosmonaut_on_a_rocket/scene.gltf'}
          model={{
            scale: 0.017,
            position: [0, -2.25, 0],
            rotation: [0, 1, 0.05],
            renderOrder: 1
          }}
          hemisphereLight={{
            intensity: 0.5,
            color: 'white'
          }}
        />
      </Suspense>
      <Preload all/>
    </Canvas>
  );
};

export default HeroCanvas;
