import {motion} from "framer-motion";

import {styles} from "../styles";
import {
  HeroCanvas
} from "./canvas";
import {itemMotion, itemRightMotion, listRightMotion} from "../utils/motion";
import React from "react";
import {GlobalContext, LanguageContext} from "../context";
import {socialsIcons} from "../constants";

const Hero = () => {
  const {darkMode, resetAnimation, isMobile} = React.useContext(GlobalContext);
  const {heroText, heroTitle} = React.useContext(LanguageContext);

  const [count, setCount] = React.useState(0)
  const [text, setText] = React.useState([])
  const [initial, setInitial] = React.useState(true)

  React.useEffect(() => {
    if (count >= heroText.length) setCount(0)
    setText(heroText[count])
  }, [count])

  React.useEffect(() => {
    setInitial(false)
  }, [resetAnimation])

  return (
    <section className={`relative w-full h-screen mx-auto ${darkMode ? 'bg-black-100' : 'bg-blue-400'}`}>
      <HeroCanvas/>
      <div
        className={`z-[5] absolute inset-0 top-[11rem] max-w-7xl mx-auto ${styles.paddingX} flex flex-row items-start gap-5`}
      >

        <motion.div
          className={'mx-auto min-w-[18rem] w-auto mt-[15rem]'}
          initial="hidden"
          animate="visible"
          variants={listRightMotion}
        >
          <motion.div
            className={'flex justify-center items-center mb-5'}
          >
            <motion.div
              key={'hero--bar-name' + resetAnimation}
              animate={{
                width: ['0%', '100%', '100%']
              }}
              transition={{
                duration: 2,
                ease: "easeInOut",
                times: [0, 1, 2],
                repeatDelay: 1,
              }}
              className={`flex`}
            >
              <h1 className={`${styles.heroHeadName} ${darkMode ? styles.darkText : styles.lightText}`}>B</h1>
              <h1 className={`${styles.heroHeadName} ${darkMode ? styles.darkText : styles.lightText}`}>R</h1>
              <h1 className={`${styles.heroHeadName} ${darkMode ? styles.darkText : styles.lightText}`}>U</h1>
              <h1 className={`${styles.heroHeadName} ${darkMode ? styles.darkText : styles.lightText}`}>N</h1>
              <h1 className={`${styles.heroHeadName} ${darkMode ? styles.darkText : styles.lightText}`}>O</h1>
              <h1 className={`${styles.heroHeadName} w-2`}/>
              <h1 className={`${styles.heroHeadName} ${darkMode ? styles.darkText : styles.lightText}`}>P</h1>
              <h1 className={`${styles.heroHeadName} ${darkMode ? styles.darkText : styles.lightText}`}>E</h1>
              <h1 className={`${styles.heroHeadName} ${darkMode ? styles.darkText : styles.lightText}`}>R</h1>
              <h1 className={`${styles.heroHeadName} ${darkMode ? styles.darkText : styles.lightText}`}>E</h1>
              <h1 className={`${styles.heroHeadName} ${darkMode ? styles.darkText : styles.lightText}`}>I</h1>
              <h1 className={`${styles.heroHeadName} ${darkMode ? styles.darkText : styles.lightText}`}>R</h1>
              <h1 className={`${styles.heroHeadName} ${darkMode ? styles.darkText : styles.lightText}`}>A</h1>
            </motion.div>
            <motion.div
              key={'hero--bar-tittle' + resetAnimation}
              animate={{
                width: ['0%', '100%', '100%']
              }}
              transition={{
                duration: 2,
                ease: "easeInOut",
                times: [0, 1, 2],
                repeatDelay: 1,
              }}
              className={`sm:min-w-[18rem] h-1 rounded-full ml-5 ${darkMode ? 'bg-tertiary' : 'bg-white'}`}
            />
          </motion.div>

          <div className={`w-fit relative overflow-hidden`}>
            <motion.div
              key={'hero-tittle' + resetAnimation}
              animate={{
                width: ['0%', '100%', '100%', '100%', '100%'],
                x: ['0%', '0%', '100%', '100%', '100%'],
              }}
              transition={{
                duration: 2,
                ease: "easeInOut",
                times: [0, 0.5, 1, 1.5, 2],
                repeatDelay: 1,
              }}
              className={`${styles.heroHeadTextDiv} relative inset-0 rounded-[2rem] rounded-r-[10rem] ${darkMode ? styles.dark : styles.light} z-[1]`}
            >
              <h1
                className={`${styles.heroHeadText} !text-transparent inset-0 z-[5] flex items-center`}
              >
                {heroTitle}
              </h1>
            </motion.div>

            <div
              className={`w-full h-full z-[0] top-0 left-0 absolute flex flex-row flex-nowrap justify-center items-center gap-5`}
            >
              <motion.h1
                key={'hero-tittle-1' + resetAnimation}
                animate={{
                  opacity: [0, 0, 1, 1, 1],
                  filter: ["blur(1px)", "blur(1px)", "blur(1px)", "blur(0px)", "blur(0px)"]
                }}
                transition={{
                  duration: 2,
                  ease: "easeInOut",
                  times: [0, 0.5, 0.8, 1, 2],
                  repeatDelay: 1,
                }}
                className={`${styles.heroHeadText} text-white inset-0 z-[2] flex items-center`}
              >
                {heroTitle}
              </motion.h1>
            </div>
          </div>

          <div className={`w-fit relative overflow-hidden`}>
            <motion.div
              key={'hero-text' + resetAnimation + count}
              animate={{
                width: ['0%', '100%', '100%', '100%', '100%'],
                x: ['0%', '0%', '100%', '100%', '100%'],
              }}
              onAnimationComplete={() => {
                setInitial(false)
                setTimeout(() => setCount(count + 1), 2500)
              }
              }
              transition={{
                duration: 2,
                ease: "easeInOut",
                times: [0, 0.5, 1, 1.5, 2],
                repeatDelay: 1,
                delay: initial ? 2 : 0
              }}
              className={`${styles.heroHeadTextDiv} relative inset-0 rounded-[2rem] rounded-r-[10rem] ${darkMode ? styles.dark : styles.light} z-[1]`}
            >
              <h1
                className={`${styles.heroHeadText} !text-transparent inset-0 z-[5] flex items-center`}
              >
                <div className="w-10 h-7 relative mr-5">

                </div>
                {text?.title}
              </h1>
            </motion.div>

            <div
              className={`w-full h-full z-[0] top-0 left-0 absolute flex flex-row flex-nowrap justify-center items-center gap-5`}
            >
              <motion.h1
                key={'hero-text-1' + resetAnimation + count}
                animate={{
                  opacity: [0, 0, 1, 1, 1],
                  filter: ["blur(1px)", "blur(1px)", "blur(1px)", "blur(0px)", "blur(0px)"]
                }}
                transition={{
                  duration: 2,
                  ease: "easeInOut",
                  times: [0, 0.5, 0.8, 1, 2],
                  repeatDelay: 1,
                  delay: initial ? 2 : 0
                }}
                className={`${styles.heroHeadText} text-white inset-0 z-[2] flex items-center justify-center`}
              >
                <div className="w-10 h-7 relative mr-5">
                  <img
                    src={text?.icon}
                    alt="flag"
                    className="h-auto w-auto flex-shrink-0"
                  />
                </div>
                {text?.title}
              </motion.h1>
            </div>
          </div>


          <div
            className={'flex justify-center items-center mt-5'}
          >
            <motion.div
              key={'socials-tittle' + resetAnimation}
              className={`flex-1 h-1 rounded-full mr-5 ${darkMode ? 'bg-tertiary' : 'bg-white'}`}
              variants={itemMotion(2)}
            />
            <div
              key={'socials-bar-name'}
              className={`flex`}
            >
              {socialsIcons.map((obj, index) => (
                <motion.button
                  key={'socials' + resetAnimation + index}
                  whileHover={{
                    scale: 1.2,
                  }}
                  whileTap={{
                    scale: 0.9
                  }}
                  variants={itemRightMotion(2)}
                  transition={{type: "spring", stiffness: 400, damping: 15}}
                  className={`flex mt-auto w-full items-center justify-center rounded-full text-base text-white focus:outline-none ${darkMode ? 'hover:bg-gray-700' : 'hover:bg-white'}`}
                >
                  <a href={obj.href} target="_blank" className="flex items-center p-2">
                    <img
                      src={obj.icon}
                      alt="flag"
                      className="block h-auto w-7 flex-shrink-0"
                    />
                  </a>
                </motion.button>
              ))}
            </div>
          </div>
        </motion.div>
      </div>

      {isMobile ? <></> : (
        <div className="absolute xs:bottom-10 bottom-32 w-full flex justify-center items-center">
          <div
            className={`w-[1.5rem] h-[2.25rem] rounded-full border-2 ${darkMode ? 'border-tertiary' : 'border-white'} flex justify-center items-start`}>
            <motion.div
              animate={{
                y: ['30%', '90%', '90%'],
                opacity: [1, 1, 0]
              }}
              transition={{
                duration: 2,
                repeat: Infinity,
                repeatType: "loop",
              }}
              className={`w-[0.125rem] h-[0.5rem] rounded-full ${darkMode ? 'bg-tertiary' : 'bg-white'}`}
            />
          </div>
        </div>
      )}
    </section>
  );
};

export default Hero;
