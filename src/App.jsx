import React, {useEffect, useState} from "react";
import {BrowserRouter} from "react-router-dom";
import {GlobalContext, GlobalProvider, LanguageProvider} from "./context";
import {
  About,
  Contact,
  Experience,
  Feedbacks,
  Hero,
  Navbar,
  Tech,
  Works,
} from "./components";
import {CosmonautCanvas, HeroCanvas, SpaceStars} from "./components/canvas";

const App = () => {
  return (
    <BrowserRouter>
      <GlobalProvider>
        <LanguageProvider>
          <div className="relative z-0 bg-secondary">
            <div>
              <Navbar/>
              <div style={{ position: 'relative' }}>
                <Hero />
                <SpaceStars/>
              </div>
            </div>


            {/*<div className="relative z-0">*/}
              {/*<About/>*/}
              {/*<Experience/>*/}
              {/*<Tech/>*/}
              {/*<Works/>*/}
              {/*<Contact/>*/}
              {/*<StarsCanvas/>*/}
            {/*</div>*/}
          </div>
        </LanguageProvider>
      </GlobalProvider>
    </BrowserRouter>
  );
};

export default App;
