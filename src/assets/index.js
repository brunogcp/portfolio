import logo from "./logo.png";
import backend from "./backend.png";
import creator from "./creator.png";
import mobile from "./mobile.png";
import web from "./web.png";
import github from "./github.png";
import menu from "./menu.svg";
import menuDark from "./menuDark.svg";
import close from "./close.svg";
import closeDark from "./closeDark.svg";

import css from "./tech/css.png";
import docker from "./tech/docker.png";
import figma from "./tech/figma.png";
import git from "./tech/git.png";
import html from "./tech/html.png";
import javascript from "./tech/javascript.png";
import mongodb from "./tech/mongodb.png";
import nodejs from "./tech/nodejs.png";
import reactjs from "./tech/reactjs.png";
import redux from "./tech/redux.png";
import tailwind from "./tech/tailwind.png";
import typescript from "./tech/typescript.png";
import threejs from "./tech/threejs.svg";

import meta from "./company/meta.png";
import shopify from "./company/shopify.png";
import starbucks from "./company/starbucks.png";
import tesla from "./company/tesla.png";

import carrent from "./carrent.png";
import jobit from "./jobit.png";
import tripguide from "./tripguide.png";

import moon from "./moon.svg"
import sun from './sun.svg'

import brazilianFlag from './brazilianFlag.svg'
import usaFlag from './usaFlag.svg'

import linkedin from './linkedin.png'

import frontEnd from './frontEnd.png'
import iphone from './iphone.png'
import arduino from './arduino.png'
import automation from './automation.png'
import alexa from './alexa.png'
import ai from './ai.png'

export {
  iphone,
  arduino,
  automation,
  alexa,
  ai,
  frontEnd,
  linkedin,
  menuDark,
  closeDark,
  usaFlag,
  brazilianFlag,
  sun,
  moon,
  logo,
  backend,
  creator,
  mobile,
  web,
  github,
  menu,
  close,
  css,
  docker,
  figma,
  git,
  html,
  javascript,
  mongodb,
  nodejs,
  reactjs,
  redux,
  tailwind,
  typescript,
  threejs,
  meta,
  shopify,
  starbucks,
  tesla,
  carrent,
  jobit,
  tripguide,
};
