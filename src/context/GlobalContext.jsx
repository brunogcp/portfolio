import React, {useEffect, useState} from 'react';
import {brazilianFlag, usaFlag} from "../assets";

const GlobalContext = React.createContext({
  darkMode: true,
  setDarkMode: () => {},
  national: true,
  setNational: () => {},
  flag: undefined,
  setFlag: () => {},
  screenSize: undefined,
});

export const GlobalProvider = ({ children }) => {
  const [isMobile, setIsMobile] = useState(false);
  const [darkMode, setDarkMode] = React.useState(true);
  const [national, setNational] = React.useState(true);
  const [flag, setFlag] = React.useState({
    svg: brazilianFlag,
    name: 'BR'
  })
  const [screenSize, setScreenSize] = React.useState(null);
  const [resetAnimation, setResetAnimation] = useState(false);


  React.useEffect(() => {
    const mediaQuery = window.matchMedia("(max-width: 500px)");
    setIsMobile(mediaQuery.matches);
    const handleMediaQueryChange = (event) => {
      setIsMobile(event.matches);
    };
    mediaQuery.addEventListener("change", handleMediaQueryChange);

    const handleResize = () => {
      const xs = window.matchMedia("(max-width: 450px)").matches;
      const sm = window.matchMedia("(min-width: 451px) and (max-width: 640px)").matches;
      const md = window.matchMedia("(min-width: 641px) and (max-width: 768px)").matches;
      const lg = window.matchMedia("(min-width: 769px) and (max-width: 1024px)").matches;
      const xl = window.matchMedia("(min-width: 1025px) and (max-width: 1280px)").matches;

      setScreenSize({ xs, sm, md, lg, xl });
    };

    handleResize();

    window.addEventListener("resize", handleResize);

    return () => {
      mediaQuery.removeEventListener("change", handleMediaQueryChange);
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const toggleDarkMode = () => {
    setDarkMode(!darkMode);
  };

  const toggleNational = () => {
    setNational(!national)
  };

  React.useEffect(() => {
    if (!national) {
      setFlag({
        svg: usaFlag,
        name: 'USA'
      })
    } else {
      setFlag({
        svg: brazilianFlag,
        name: 'BR'
      })
    }
  }, [national])

  return (
    <GlobalContext.Provider value={{ isMobile, darkMode, toggleDarkMode, national, toggleNational, flag, screenSize, resetAnimation, setResetAnimation }}>
      {children}
    </GlobalContext.Provider>
  );
};

export default GlobalContext;
