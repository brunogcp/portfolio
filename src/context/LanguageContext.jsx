import React from 'react';

import {
  NavLinks,
  HeroText,
  HeroTitle
} from "../constants";
import GlobalContext from "./GlobalContext";

const LanguageContext = React.createContext({
  navLinks: undefined
});

export const LanguageProvider = ({ children }) => {
  const {national} = React.useContext(GlobalContext);
  const [navLinks, setNavLinks] = React.useState(NavLinks.pt)
  const [cvLabel, setCvLabel] = React.useState('Baixar Currículo')
  const [heroText, setHeroText] = React.useState(HeroText.pt)
  const [heroTitle, setHeroTitle] = React.useState(HeroTitle.pt)

  React.useEffect(() => {
    if (!national) {
      setNavLinks(NavLinks.eng)
      setCvLabel('Download CV')
      setHeroText(HeroText.eng)
      setHeroTitle(HeroTitle.eng)
    } else {
      setNavLinks(NavLinks.pt)
      setCvLabel('Baixar Currículo')
      setHeroText(HeroText.pt)
      setHeroTitle(HeroTitle.pt)
    }
  }, [national])

  return (
    <LanguageContext.Provider value={{ navLinks, cvLabel, heroText, heroTitle }}>
      {children}
    </LanguageContext.Provider>
  );
};

export default LanguageContext;
