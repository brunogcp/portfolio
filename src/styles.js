const styles = {
  paddingX: "px-5",
  paddingY: "sm:py-16 py-6",
  padding: "sm:px-16 px-6 sm:py-16 py-10",

  dark: "bg-gradient-to-b from-gray-900 to-gray-800",
  light: "bg-gradient-to-b from-blue-400 to-blue-100",

  darkText: 'text-tertiary',
  lightText: 'text-white',

  heroHeadText:
    "font-black text-white lg:text-[80px] sm:text-[60px] xs:text-[50px] text-[40px] lg:leading-[98px]",

  heroHeadTextDiv:
    "font-black text-white lg:h-[100px] sm:h-[80px] xs:h-[70px] h-[60px]",

  heroHeadName:
    "font-black text-tertiary lg:text-[40px] sm:text-[30px] xs:text-[25px] text-[20px] lg:leading-[50px] mt-2",

  heroSubText:
    "text-[#dfd9ff] font-medium lg:text-[30px] sm:text-[26px] xs:text-[20px] text-[16px] lg:leading-[40px]",

  sectionHeadText:
    "text-white font-black md:text-[60px] sm:text-[50px] xs:text-[40px] text-[30px]",
  sectionSubText:
    "sm:text-[18px] text-[14px] text-secondary uppercase tracking-wider",
};

export { styles };
