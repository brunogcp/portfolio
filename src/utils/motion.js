export const textVariant = (delay) => {
  return {
    hidden: {
      y: -50,
      opacity: 0,
    },
    show: {
      y: 0,
      opacity: 1,
      transition: {
        type: "spring",
        duration: 1.25,
        delay: delay,
      },
    },
  };
};

export const fadeIn = (direction, type, delay, duration) => {
  return {
    hidden: {
      x: direction === "left" ? 100 : direction === "right" ? -100 : 0,
      y: direction === "up" ? 100 : direction === "down" ? -100 : 0,
      opacity: 0,
    },
    show: {
      x: 0,
      y: 0,
      opacity: 1,
      transition: {
        type: type,
        delay: delay,
        duration: duration,
        ease: "easeOut",
      },
    },
  };
};

export const zoomIn = (delay, duration) => {
  return {
    hidden: {
      scale: 0,
      opacity: 0,
    },
    show: {
      scale: 1,
      opacity: 1,
      transition: {
        type: "tween",
        delay: delay,
        duration: duration,
        ease: "easeOut",
      },
    },
  };
};

export const slideIn = (direction, type, delay, duration) => {
  return {
    hidden: {
      x: direction === "left" ? "-100%" : direction === "right" ? "100%" : 0,
      y: direction === "up" ? "100%" : direction === "down" ? "100%" : 0,
    },
    show: {
      x: 0,
      y: 0,
      transition: {
        type: type,
        delay: delay,
        duration: duration,
        ease: "easeOut",
      },
    },
  };
};

export const staggerContainer = (staggerChildren, delayChildren) => {
  return {
    hidden: {},
    show: {
      transition: {
        staggerChildren: staggerChildren,
        delayChildren: delayChildren || 0,
      },
    },
  };
};

export const navMotion = {
  visible: {
    y: 0,
    transition: {
      duration: 1
    }
  },
  hidden: {y: '-100%'},
}

export const listMotion = {
  visible: {
    opacity: 1,
    transition: {
      when: "beforeChildren",
      staggerChildren: 0.3,
    },
  },
  hidden: {
    opacity: 0,
    transition: {
      when: "afterChildren",
    },
  },
}

export const listRightMotion = {
  visible: {
    opacity: 1,
    transition: {
      when: "beforeChildren",
    },
  },
  hidden: {
    opacity: 0,
    transition: {
      when: "afterChildren",
    },
  },
}

export const itemMotion = (delay = 0) => {
  return {
    visible: {
      opacity: 1, x: 0, transition: {
        delay: delay / 2.5,
        duration: 0.5,
        ease: "easeOut",
      },
      filter: "blur(0px)"
    },
    hidden: {opacity: 0, x: -100, filter: "blur(7px)"},
  }
}

export const itemRightMotion = (delay = 0) => {
  return {
    visible: {
      opacity: 1, x: 0, transition: {
        delay: delay / 2.5,
        duration: 0.5,
        ease: "easeOut",
      }
    },
    hidden: {opacity: 0, x: 100},
  }
}

export const itemDownMotion = (delay = 0) => {
  return {
    visible: {
      opacity: 1, y: 0, transition: {
        delay: delay / 2.5,
        duration: 0.5,
        ease: "easeOut",
      }
    },
    hidden: {opacity: 0, y: -25},
  }
}

export const menuListMotion = (delay = 0) => {
  return {
    visible: {
      opacity: 1, x: 0, scale: 1, transition: {
        delay: delay / 7,
        duration: 0.25,
        ease: "easeOut",
      },
      filter: "blur(0px)"
    },
    hidden: {opacity: 0, x: -100, scale: 0.3, filter: "blur(20px)"},
  }
}

export const menuCvMotion = (delay = 0) => {
  return {
    visible: {
      opacity: 1, scale: 1, transition: {
        delay: delay / 7,
        duration: 0.25,
        ease: "easeOut",
      },
      filter: "blur(0px)"
    },
    hidden: {opacity: 0, scale: 0.3, filter: "blur(20px)"},
  }
}

export const textMotion = (delay = 0) => {
  return {
    visible: {
      scale: [1, 2, 2, 1, 1],
      rotate: [0, 0, 180, 180, 0],
      borderRadius: ["0%", "0%", "50%", "50%", "0%"]
    },
    hidden: {opacity: 0, width: 0}
  }
}