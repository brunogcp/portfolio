import {heroPt, heroTitlePt} from './pt-br'
import {heroEng, heroTitleEng} from './eng-us'

export const HeroText = {
  pt: heroPt,
  eng: heroEng
}

export const HeroTitle = {
  pt: heroTitlePt,
  eng: heroTitleEng
}
