import {ai, alexa, arduino, automation, backend, brazilianFlag, frontEnd, iphone, logo} from "../../assets";


export const heroEng = [
  {
    icon: logo,
    title: "Front-End",
  },
  {
    icon: logo,
    title: "Back-End",
  },
  {
    icon: logo,
    title: "Mobile",
  },
  {
    icon: logo,
    title: "Automation",
  },
  {
    icon: logo,
    title: "IOT - ESP-32",
  },
  {
    icon: logo,
    title: "Alexa - AWS",
  },
  {
    icon: logo,
    title: "IA - GPT 4",
  },
];

export const heroTitleEng = 'Developer'