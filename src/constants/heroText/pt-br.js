import {ai, alexa, arduino, automation, backend, frontEnd, iphone, logo, web} from "../../assets";

export const heroPt = [
  {
    icon: logo,
    title: "Front-End",
  },
  {
    icon: logo,
    title: "Back-End",
  },
  {
    icon: logo,
    title: "Aplicativos",
  },
  {
    icon: logo,
    title: "Automação",
  },
  {
    icon: logo,
    title: "IOT - ESP-32",
  },
  {
    icon: logo,
    title: "Alexa - AWS",
  },
  {
    icon: logo,
    title: "IA - GPT 4",
  },
];

export const heroTitlePt = 'Desenvolvedor'