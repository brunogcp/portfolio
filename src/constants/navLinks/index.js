import {navLinksPt} from './pt-br'
import {navLinksEng} from './eng-us'

export const NavLinks = {
  pt: navLinksPt,
  eng: navLinksEng
}
